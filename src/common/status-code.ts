/**
 * @file Defines the StatusCode enum that is used to
 *       give system status codes.
 * @since 09/01/2018
 * @copyright Jacob Heater <jacobheater@gmail.com>
 */

export enum StatusCode {
    OK = 0,
    Error = 1,
    ArgumentInvalid = 2,
    ArgvInvalid = 3
}
