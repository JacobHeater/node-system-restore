/**
 * @file Defines the RestorePointType enum.
 * @since 09/01/2018
 * @copyright Jacob Heater <jacobheater@gmail.com>
 */

export enum RestorePointType {
    APPLICATION_INSTALL = 0,
    APPLICATION_UNINSTALL = 1,
    DEVICE_DRIVER_INSTALL = 10,
    MODIFY_SETTINGS = 12,
    CANCELLED_OPERATION = 13
}
